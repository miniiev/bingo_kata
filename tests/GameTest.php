<?php

declare(strict_types=1);

namespace Ruslan\BingoKata\Test;

use PHPUnit\Framework\TestCase;
use Ruslan\BingoKata\Game;
use Ruslan\BingoKata\Number;
use Ruslan\BingoKata\Number\Generator;
use Ruslan\BingoKata\Player;

class GameTest extends TestCase
{
    private const MIN_NUMBER_GENERATION_BOUND = 1;
    private const MAX_NUMBER_GENERATION_BOUND = 75;

    public function testCreate()
    {
        $this->assertInstanceOf(Game::class, $this->createGame($this->createMock(Generator\Factory::class)));
    }

    public function testDisallowedPlayersCount()
    {
        $this->expectException(\RuntimeException::class);

        $player = $this->createMock(Player::class)
            ->method('processCalledNumber')
            ->method('getCard');

        $game = $this->createGame($this->createMock(Generator\Factory::class));
        $game->registerPlayer($player);

        $game->callNextNumber();
    }

    public function testCreateGenerator()
    {
        $generatorFactory = $this->createMock(Generator\Factory::class);
        $generatorFactory->expects($this->once())
            ->method('create')
            ->with(self::MIN_NUMBER_GENERATION_BOUND, self::MAX_NUMBER_GENERATION_BOUND);

        new Game($generatorFactory);
    }

    public function testNumberGeneration()
    {
        $player1 = $this->createMock(Player::class);
        $player1->method('processCalledNumber');

        $player2 = $this->createMock(Player::class);
        $player2->method('processCalledNumber');

        $generator = $this->createMock(Generator::class);
        $generator->expects($this->once())->method('generateUnique');

        $generatorFactory = $this->createMock(Generator\Factory::class);
        $generatorFactory->expects($this->once())->method('create')->willReturn($generator);

        $game = new Game($generatorFactory);
        $game->registerPlayer($player1);
        $game->registerPlayer($player2);

        $game->callNextNumber();
    }

    public function testProcessPlayerGeneratedNumber()
    {
        $number = $this->createMock(Number::class);

        $player1 = $this->createMock(Player::class);
        $player1->expects($this->once())->method('processCalledNumber')->with($number);

        $player2 = $this->createMock(Player::class);
        $player2->expects($this->once())->method('processCalledNumber')->with($number);

        $generator = $this->createMock(Generator::class);
        $generator->method('generateUnique')->willReturn($number);

        $generatorFactory = $this->createMock(Generator\Factory::class);
        $generatorFactory->expects($this->once())->method('create')->willReturn($generator);

        $game = new Game($generatorFactory);
        $game->registerPlayer($player1);
        $game->registerPlayer($player2);

        $game->callNextNumber();
    }

    private function createGame($generatorFactory): Game
    {
        return new Game($generatorFactory);
    }
}