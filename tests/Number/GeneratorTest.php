<?php

declare(strict_types=1);

namespace Ruslan\BingoKata\Test\Number;

use PHPUnit\Framework\TestCase;
use Ruslan\BingoKata\Number;

class GeneratorTest extends TestCase
{
    /**
     * @dataProvider validBoundsProvider
     */
    public function testCreateSuccess($valuesLowerBound, $valuesUpperBound)
    {
        $this->assertInstanceOf(Number\Generator::class, $this->createGenerator($valuesLowerBound, $valuesUpperBound));
    }

    /**
     * @dataProvider invalidBoundsIntervalProvider
     */
    public function testCreateFailedBoundsInterval($valuesLowerBound, $valuesUpperBound)
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->createGenerator($valuesLowerBound, $valuesUpperBound);
    }

    /**
     * @dataProvider generatedNumbersProvider
     */
    public function testGeneratedNumbers($valuesLowerBound, $valuesUpperBound, $expectedNumbersValues)
    {
        $generatedNumbersValues = [];

        $generator = $this->createGenerator($valuesLowerBound, $valuesUpperBound);

        while ($generator->canGenerate()) {
            $number = $generator->generateUnique();

            $generatedNumbersValues[] = $number->getValue();
        }

        $this->assertEqualsCanonicalizing($expectedNumbersValues, $generatedNumbersValues);
    }

    private function createGenerator($valuesLowerBound, $valuesUpperBound): Number\Generator
    {
        return new Number\Generator($valuesLowerBound, $valuesUpperBound);
    }

    public function validBoundsProvider(): array
    {
        return [
            [1, 10],
            [2, 7],
            [0, 1],
            [-1, 1],
            [-5, 0],
        ];
    }

    public function invalidBoundsIntervalProvider(): array
    {
        return [
            [1, -1],
            [20, 7],
            [0, 0],
            [1, 1],
        ];
    }

    public function generatedNumbersProvider()
    {
        return [
            [1, 7, [1, 2, 3, 4, 5, 6, 7]],
            [-1, 2, [-1, 0, 1, 2]],
        ];
    }
}