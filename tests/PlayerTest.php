<?php

declare(strict_types=1);

namespace Ruslan\BingoKata\Test;

use PHPUnit\Framework\TestCase;
use Ruslan\BingoKata\Card;
use Ruslan\BingoKata\Number;
use Ruslan\BingoKata\Player;

class PlayerTest extends TestCase
{
    public function testCreate()
    {
        $player = $this->createPlayer(
            'Test Player', $this->createMock(Card::class), $this->createMock(Player\GameContext::class)
        );

        $this->assertInstanceOf(Player::class, $player);
    }

    public function testCardCheck()
    {
        $number = $this->createMock(Number::class);

        $card = $this->createMock(Card::class);
        $card->expects($this->once())->method('check')->with($number);

        $player = $this->createPlayer('Test Player', $card, $this->createMock(Player\GameContext::class));
        $player->processCalledNumber($number);
    }

    public function testBingo()
    {
        $card = $this->createMock(Card::class);
        $card->method('check')->willReturn(true);

        $gameContext = $this->createMock(Player\GameContext::class);
        $gameContext->expects($this->once())->method('bingo');

        $player = $this->createPlayer('Test Player', $card, $gameContext);
        $player->processCalledNumber($this->createMock(Number::class));
    }

    private function createPlayer($name, $card, $gameContext): Player
    {
        return new Player($name, $card, $gameContext);
    }
}