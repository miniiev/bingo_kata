<?php

declare(strict_types=1);

namespace Ruslan\BingoKata\Test;

use PHPUnit\Framework\TestCase;
use Ruslan\BingoKata\Number;

class NumberTest extends TestCase
{
    /**
     * @dataProvider valuesProvider
     */
    public function testCreate($value)
    {
        $number = $this->createNumber($value);

        $this->assertInstanceOf(Number::class, $number);
        $this->assertEquals($value, $number->getValue());
    }

    private function createNumber($value): Number
    {
        return new Number($value);
    }

    public function valuesProvider(): array
    {
        return [
            [0],
            [2],
            [-1],
            [15],
        ];
    }
}