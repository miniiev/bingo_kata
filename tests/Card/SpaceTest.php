<?php

declare(strict_types=1);

namespace Ruslan\BingoKata\Test\Card;

use PHPUnit\Framework\TestCase;
use Ruslan\BingoKata\Card\Space;
use Ruslan\BingoKata\Number;

class SpaceTest extends TestCase
{
    public function testCreate()
    {
        $number = $this->createMock(Number::class);
        $space  = $this->createSpace($number);

        $this->assertInstanceOf(Space::class, $space);
    }

    /**
     * @dataProvider numberValuesProvider
     */
    public function testContainsNumber($numberValue)
    {
        $number = $this->createMock(Number::class);
        $number->method('getValue')->willReturn($numberValue);
        $number->method('equals')->willReturn(true);

        $space = $this->createSpace($number);

        $this->assertTrue($space->containsNumber($number));
    }

    private function createSpace(Number $number)
    {
        return new Space($number);
    }

    public function numberValuesProvider()
    {
        return [
            [-1],
            [12],
            [0],
            [22],
        ];
    }
}