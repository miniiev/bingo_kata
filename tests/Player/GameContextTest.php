<?php

declare(strict_types=1);

namespace Ruslan\BingoKata\Test\Player;

use PHPUnit\Framework\TestCase;
use Ruslan\BingoKata\Game;
use Ruslan\BingoKata\Player;

class GameContextTest extends TestCase
{
    public function testBingo()
    {
        $player = $this->createMock(Player::class);

        $game = $this->getMockBuilder(Game::class)
            ->disableOriginalConstructor()
            ->setMethods(['processPlayersBingo'])
            ->getMock();

        $game->expects($this->once())
            ->method('processPlayersBingo')
            ->with($player);

        $gameContext = new Player\GameContext($game);
        $gameContext->bingo($player);
    }
}