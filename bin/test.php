<?php

require_once '../vendor/autoload.php';

$numberGeneratorFactory = new \Ruslan\BingoKata\Number\Generator\Factory();
$cardGeneratorFactory   = new \Ruslan\BingoKata\Card\Generator\Factory();

$game = new \Ruslan\BingoKata\Game($numberGeneratorFactory);
$playerGameContext = new \Ruslan\BingoKata\Player\GameContext($game);

$cardGenerator = $cardGeneratorFactory->create($numberGeneratorFactory);

$player1 = new \Ruslan\BingoKata\Player('Player 1', $cardGenerator->generate(), $playerGameContext);
$game->registerPlayer($player1);

$player2 = new \Ruslan\BingoKata\Player('Player 2', $cardGenerator->generate(), $playerGameContext);
$game->registerPlayer($player2);

$player3 = new \Ruslan\BingoKata\Player('Player 3', $cardGenerator->generate(), $playerGameContext);
$game->registerPlayer($player3);

$calledNumbersCount = 0;

while (!$game->isFinished()) {
    $game->callNextNumber();
    $calledNumbersCount++;
}

echo 'Winner is ' . $game->getWinner()->getName() . '. Numbers count: '.$calledNumbersCount;