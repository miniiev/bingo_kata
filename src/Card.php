<?php

declare(strict_types=1);

namespace Ruslan\BingoKata;

use Ruslan\BingoKata\Card\Space;

class Card
{
    /** @var Space[][] */
    private $spacesMatrix;

    /**
     * @param Space[][] $spacesMatrix
     */
    public function __construct(array $spacesMatrix)
    {
        $this->spacesMatrix = $spacesMatrix;
    }

    /**
     * @param Number $number
     * @return bool
     */
    public function check(Number $number): bool
    {
        foreach ($this->spacesMatrix as $spacesColumn) {
            foreach ($spacesColumn as $space) {
                if (!is_null($space) && $space->containsNumber($number)) {
                    $space->mark();
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function hasUnmarkedSpaces(): bool
    {
        foreach ($this->spacesMatrix as $spacesColumn) {
            foreach ($spacesColumn as $space) {
                if (!is_null($space) && !$space->isMarked()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return Space[]
     */
    public function getMarkedSpaces(): array
    {
        $spaces = [];

        foreach ($this->spacesMatrix as $spacesColumn) {
            foreach ($spacesColumn as $space) {
                if (!is_null($space) && $space->isMarked()) {
                    $spaces[] = $space;
                }
            }
        }

        return $spaces;
    }
}