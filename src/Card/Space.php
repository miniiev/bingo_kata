<?php

declare(strict_types=1);

namespace Ruslan\BingoKata\Card;

use Ruslan\BingoKata\Number;

class Space
{
    /** @var Number */
    private $number;

    /** @var bool */
    private $isMarked = false;

    /**
     * @param Number $number
     */
    public function __construct(Number $number)
    {
        $this->number = $number;
    }

    /**
     * @param Number $number
     * @return bool
     */
    public function containsNumber(Number $number): bool
    {
        return $this->number->equals($number);
    }

    /**
     * @return bool
     */
    public function isMarked(): bool
    {
        return $this->isMarked;
    }

    public function mark(): void
    {
        $this->isMarked = true;
    }
}