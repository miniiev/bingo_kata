<?php

declare(strict_types=1);

namespace Ruslan\BingoKata\Card;

use Ruslan\BingoKata\Card;
use Ruslan\BingoKata\Number\Generator\Factory as NumberGeneratorFactory;

class Generator
{
    private const COLUMNS_BOUNDS = [
        ['lower' => 1, 'upper' => 15],
        ['lower' => 16, 'upper' => 30],
        ['lower' => 31, 'upper' => 45],
        ['lower' => 46, 'upper' => 60],
        ['lower' => 61, 'upper' => 75],
    ];

    /** @var NumberGeneratorFactory */
    private $numberGeneratorFactory;

    /**
     * @param NumberGeneratorFactory $numberGeneratorFactory
     */
    public function __construct(NumberGeneratorFactory $numberGeneratorFactory)
    {
        $this->numberGeneratorFactory = $numberGeneratorFactory;
    }

    /**
     * @return Card
     */
    public function generate(): Card
    {
        $cardSize = count(self::COLUMNS_BOUNDS);

        $freeSpaceIndex = (int)($cardSize / 2);

        $spacesMatrix = [];

        foreach (self::COLUMNS_BOUNDS as $columnIndex => $columnBounds) {
            $numberGenerator = $this->numberGeneratorFactory->create($columnBounds['lower'], $columnBounds['upper']);

            for ($rowIndex = 0; $rowIndex < $cardSize; $rowIndex++) {
                if ($columnIndex == $freeSpaceIndex && $rowIndex == $freeSpaceIndex) {
                    $spacesMatrix[$columnIndex][$rowIndex] = null;
                    continue;
                }

                $spacesMatrix[$columnIndex][$rowIndex] = new Space($numberGenerator->generateUnique());
            }
        }

        return new Card($spacesMatrix);
    }
}