<?php

declare(strict_types=1);

namespace Ruslan\BingoKata\Card\Generator;

use Ruslan\BingoKata\Card\Generator;
use Ruslan\BingoKata\Number\Generator\Factory as NumberGeneratorFactory;

class Factory
{
    /**
     * @param NumberGeneratorFactory $numberGeneratorFactory
     * @return Generator
     */
    public function create(NumberGeneratorFactory $numberGeneratorFactory): Generator
    {
        return new Generator($numberGeneratorFactory);
    }
}