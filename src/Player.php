<?php

declare(strict_types=1);

namespace Ruslan\BingoKata;

use Ruslan\BingoKata\Player\GameContext;

class Player
{
    /** @var string */
    private $name;

    /** @var Card */
    private $card;

    /** @var GameContext */
    private $gameContext;

    /**
     * @param string $name
     * @param Card $card
     * @param GameContext $gameContext
     */
    public function __construct(string $name, Card $card, GameContext $gameContext)
    {
        $this->name        = $name;
        $this->card        = $card;
        $this->gameContext = $gameContext;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param Number $number
     */
    public function processCalledNumber(Number $number): void
    {
        if (!$this->card->check($number)) {
            return;
        }

        if (!$this->card->hasUnmarkedSpaces()) {
            $this->gameContext->bingo($this);
        }
    }

    /**
     * @return Card
     */
    public function getCard(): Card
    {
        return $this->card;
    }
}