<?php

declare(strict_types=1);

namespace Ruslan\BingoKata;

use Ruslan\BingoKata\Number\Generator\Factory as NumberGeneratorFactory;

class Game
{
    private const MIN_GENERATED_NUMBER_VALUE = 1;
    private const MAX_GENERATED_NUMBER_VALUE = 75;

    /** @var Player[] */
    private $players = [];

    /** @var Number[] */
    private $generatedNumbers = [];

    /** @var Player */
    private $winner;

    /** @var Number\Generator */
    private $numberGenerator;

    /**
     * @param NumberGeneratorFactory $numberGeneratorFactory
     */
    public function __construct(NumberGeneratorFactory $numberGeneratorFactory)
    {
        $this->numberGenerator = $numberGeneratorFactory->create(
            self::MIN_GENERATED_NUMBER_VALUE,
            self::MAX_GENERATED_NUMBER_VALUE
        );
    }

    /**
     * @param Player $player
     */
    public function registerPlayer(Player $player): void
    {
        $this->players[] = $player;
    }

    /**
     * @throws \RuntimeException
     */
    public function callNextNumber(): void
    {
        if (count($this->players) < 2) {
            throw new \RuntimeException('Game must have at least 2 players.');
        }

        if ($this->isFinished()) {
            throw new \RuntimeException('Game has already finished.');
        }

        $number = $this->numberGenerator->generateUnique();

        $this->generatedNumbers[] = $number;

        foreach ($this->players as $player) {
            $player->processCalledNumber($number);
        }
    }

    /**
     * @param Player $player
     */
    public function processPlayersBingo(Player $player): void
    {
        if ($this->validateCard($player->getCard())) {
            $this->winner = $player;
        }
    }

    /**
     * @return bool
     */
    public function isFinished(): bool
    {
        return !is_null($this->winner);
    }

    /**
     * @return Player
     * @throws \RuntimeException
     */
    public function getWinner(): Player
    {
        if (is_null($this->winner)) {
            throw new \RuntimeException('Game have not finished yet.');
        }

        return $this->winner;
    }

    /**
     * @param Card $card
     * @return bool
     */
    private function validateCard(Card $card): bool
    {
        foreach ($card->getMarkedSpaces() as $cardSpace) {
            $isSpaceNumberFound = false;

            foreach ($this->generatedNumbers as $generatedNumber) {
                if ($cardSpace->containsNumber($generatedNumber)) {
                    $isSpaceNumberFound = true;
                    break;
                }
            }

            if (!$isSpaceNumberFound) {
                return false;
            }
        }

        return true;
    }
}