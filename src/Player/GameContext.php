<?php

declare(strict_types=1);

namespace Ruslan\BingoKata\Player;

use Ruslan\BingoKata\Game;
use Ruslan\BingoKata\Player;

class GameContext
{
    /** @var Game */
    private $game;

    /**
     * @param Game $game
     */
    public function __construct(Game $game)
    {
        $this->game = $game;
    }

    /**
     * @param Player $player
     */
    public function bingo(Player $player): void
    {
        $this->game->processPlayersBingo($player);
    }
}