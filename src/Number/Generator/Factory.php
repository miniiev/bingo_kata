<?php

declare(strict_types=1);

namespace Ruslan\BingoKata\Number\Generator;

use Ruslan\BingoKata\Number\Generator;

class Factory
{
    /**
     * @param int $valuesLowerBound
     * @param int $valuesUpperBound
     * @return Generator
     */
    public function create(int $valuesLowerBound, int $valuesUpperBound): Generator
    {
        return new Generator($valuesLowerBound, $valuesUpperBound);
    }
}