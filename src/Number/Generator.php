<?php

declare(strict_types=1);

namespace Ruslan\BingoKata\Number;

use Ruslan\BingoKata\Number;

class Generator
{
    /** @var int[] */
    private $valuesGenerationList;

    /**
     * @param int $valuesLowerBound
     * @param int $valuesUpperBound
     * @throws \InvalidArgumentException
     */
    public function __construct(int $valuesLowerBound, int $valuesUpperBound)
    {
        if ($valuesLowerBound >= $valuesUpperBound) {
            throw new \InvalidArgumentException('Values bounds are invalid.');
        }

        $this->valuesGenerationList = range($valuesLowerBound, $valuesUpperBound);
    }

    /**
     * @return bool
     */
    public function canGenerate(): bool
    {
        return !empty($this->valuesGenerationList);
    }

    /**
     * @return Number
     * @throws \RuntimeException
     */
    public function generateUnique(): Number
    {
        if (empty($this->valuesGenerationList)) {
            throw new \RuntimeException('All numbers have already been generated.');
        }

        $index = rand(0, count($this->valuesGenerationList) - 1);

        $number = new Number($this->valuesGenerationList[$index]);

        unset($this->valuesGenerationList[$index]);
        $this->valuesGenerationList = array_values($this->valuesGenerationList);

        return $number;
    }
}