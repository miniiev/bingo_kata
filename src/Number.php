<?php

declare(strict_types=1);

namespace Ruslan\BingoKata;

class Number
{
    /** @var int */
    private $value;

    /**
     * @param int $value
     */
    public function __construct(int $value)
    {
        $this->value = $value;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * @param Number $number
     * @return bool
     */
    public function equals(Number $number): bool
    {
        return $this->getValue() == $number->getValue();
    }
}